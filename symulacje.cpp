#include <GL/glut.h>
#include <math.h>
#include <unistd.h>  //usleep

#include <algorithm>
#include <iostream>
#include <list>
#include <random>

#include "libraries/przeszkody.h"
#include "libraries/punkt.h"
#include "libraries/kulki.h"
#include "libraries/stale_wektory.h"
#include "libraries/wektor3d.h"
#include "libraries/GLprzeszkody.h"
#include "libraries/GLpunkt.h"

using namespace std;

unsigned int step = 5;

list<punkt *> Punkciory;
list<kulka *> Kulki;
list<GLobject *> GLpunkty;
list<GLobject *> GLPrzeszkody;
list<przeszkoda *> Przeszkody;
random_device generator;
void (*uTikEuler)(int);
/*
void uTikEuler12(int ustep) {
  double step = (double)ustep / 1000;
  std::for_each(Punkciory.begin(), Punkciory.end(),
                [step](GLpunkt *Punkt) { Punkt->tikEuler(step); });
}

void symulacja001_punkt() {
  wektor3d Wektor(0, 0.7, 0);
  Punkciory.push_back(new GLpunkt(Wektor, Wektor, g, 1.6, 0, 0, 0));
  uTikEuler = uTikEuler12;
}

void symulacja002_punkty(int ilosc) {
  normal_distribution<double> distribution1(0, 0.7);
  uniform_real_distribution<double> distribution2(2, 4);
  for (int i = 0; i < ilosc; i++) {
    Punkciory.push_back(new GLpunkt(
        zeroWektor,
        wektor3d(distribution1(generator), distribution2(generator), 0), g,
        distribution2(generator) * 3));
  }
  uTikEuler = uTikEuler12;
}

void uTikEuler3(int ustep) {
  double step = (double)ustep / 1000;
  std::for_each(Punkciory.begin(), Punkciory.end(), [step](punkt *Punkt) {
    Punkt->tikEuler(step);
    std::for_each(Przeszkody.begin(), Przeszkody.end(),
                  [Punkt](przeszkoda *Przeszkoda) {
                    if (Przeszkoda->zderzenie(*Punkt)) {
                      Przeszkoda->zderzonyTik(*Punkt);
                    }
                  });
  });
  std::for_each(Kulki.begin(), Kulki.end(), [step](kulka *Kulka) {
    Kulka->tikEuler(step);
    std::for_each(Przeszkody.begin(), Przeszkody.end(),
                  [Kulka](przeszkoda *Przeszkoda) {
                    if (Przeszkoda->zderzenie(*Kulka)) {
                      Przeszkoda->zderzonyTik(*Kulka);
                    }
                  });
  });
}

void symulacja003_punkty(int ilosc) {
  normal_distribution<double> distribution1(0, 0.7);
  uniform_real_distribution<double> distribution2(3, 5);
  uniform_real_distribution<double> distribution3(0, 1);
  GLklatka2d *klatka = new GLklatka2d(-0.9, 0.9, 0.9, -0.9);
  GLsfera *sfera = new GLsfera(0.7, wektor3d(0, -0.9, 0), 0.9);
  Przeszkody.push_back(klatka);
  GLPrzeszkody.push_back(klatka);
  Przeszkody.push_back(sfera);
  GLPrzeszkody.push_back(sfera);
  for (int i = 0; i < ilosc; i++) {
    Punkciory.push_back(new GLpunkt(
        zeroWektor,
        wektor3d(distribution1(generator), distribution2(generator), 0), g,
        distribution2(generator) * 3, distribution3(generator),
        distribution3(generator), distribution3(generator)));
  }
  uTikEuler = uTikEuler4;
}
*/

void uTikEuler4(int ustep) {
  double step = (double)ustep / 1000;
  std::for_each(Punkciory.begin(), Punkciory.end(), [step](punkt *Punkt) {
    Punkt->tikEuler(step);
    std::for_each(Przeszkody.begin(), Przeszkody.end(),
                  [Punkt](przeszkoda *Przeszkoda) {
                    if (Przeszkoda->zderzenie(*Punkt)) {
                      Przeszkoda->zderzonyTik(*Punkt);
                    }
                  });
  });
  std::for_each(Kulki.begin(), Kulki.end(), [step](kulka *Kulka) {
    Kulka->tikEuler(step);
    std::for_each(Przeszkody.begin(), Przeszkody.end(),
                  [Kulka](przeszkoda *Przeszkoda) {
                    if (Przeszkoda->zderzenie(*Kulka)) {
                      Przeszkoda->zderzonyTik(*Kulka);
                    }
                  });
  });
}

void symulacja004_punkty(int ilosc) {
  normal_distribution<double> distribution1(0, 0.7);
  uniform_real_distribution<double> distribution2(3, 5);
  uniform_real_distribution<double> distribution3(0, 1);
  GLklatka2d *klatka = new GLklatka2d(-0.9, 0.9, 0.9, -0.9);
  GLsfera *sfera = new GLsfera(0.7, wektor3d(0, -0.9, 0), 0.9);
  Przeszkody.push_back(klatka);
  GLPrzeszkody.push_back(klatka);
  Przeszkody.push_back(sfera);
  GLPrzeszkody.push_back(sfera);
  for (int i = 0; i < ilosc; i++) {
    GLpunkt *Punkt = new GLpunkt(
        zeroWektor,
        wektor3d(distribution1(generator), distribution2(generator), 0), g,
        distribution2(generator) * 3, distribution3(generator),
        distribution3(generator), distribution3(generator));
    Punkciory.push_back(Punkt);
    GLpunkty.push_back(Punkt);
    GLkulka *Kulka = new GLkulka(
        zeroWektor,
        wektor3d(distribution1(generator), distribution2(generator), 0), g,
        distribution2(generator) * 3, distribution3(generator)/10, distribution3(generator),
        distribution3(generator), distribution3(generator));
    Kulki.push_back(Kulka);
    GLpunkty.push_back(Kulka);

  }
  uTikEuler = uTikEuler4;
}

void Display() {
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);
  for_each(GLpunkty.begin(), GLpunkty.end(),
           [](GLobject *Punkt) { Punkt->paintObject(); });
  for_each(GLPrzeszkody.begin(), GLPrzeszkody.end(),
           [](GLobject *Przeszkoda) { Przeszkoda->paintObject(); });

  glFlush();
  glutSwapBuffers();

  glutTimerFunc(step, uTikEuler, step);
  glutPostRedisplay();
}

void Reshape(int width, int height) {
  // generowanie sceny 3D
  Display();
}

int main(int argc, char *argv[]) {
  // inicjalizacja punktów
  // symulacja001_punkt();
  symulacja004_punkty(20);
  // inicjalizacja biblioteki GLUT
  glutInit(&argc, argv);

  // inicjalizacja bufora ramki
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

  // rozmiary głównego okna programu
  glutInitWindowSize(700, 700);

  // utworzenie głównego okna programu
  glutCreateWindow("Symulacja - punkt");

  // dołączenie funkcji generującej scenę 3D
  glutDisplayFunc(Display);

  // dołączenie funkcji wywoływanej przy zmianie rozmiaru okna
  glutReshapeFunc(Reshape);

  // wprowadzenie programu do obsługi pętli komunikatów
  glutMainLoop();

  // wektor3d Wektor(1, 1.7, -3);
  // Wektor.wydrukuj();
  // (Wektor + wektor3d(1, 1, 1)).wydrukuj();
  // (2 * Wektor).wydrukuj();
  // punkt Punkcior(Wektor, Wektor, Wektor, 1.6);
  // punkt Punkcior2(Wektor);
  // Punkcior.wydrukuj();
  // Punkcior2.wydrukuj();
  // Punkcior.tikEuler(0.1);
  // Punkcior.wydrukuj();
  return 1;
}