#ifndef GLOBJECTS_H
#define GLOBJECTS_H

#include <GL/freeglut.h>

#include "przeszkody.h"

class GLobject {
 public:
  virtual void paintObject() const {}
};
#endif /* GLOBJECTS_H */
