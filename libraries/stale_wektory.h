#ifndef STALE_WEKTORY_H
#define STALE_WEKTORY_H
#include "wektor3d.h"

// Stałe wektory.
// Póki co jest tylko wektor grawitacji.

extern const wektor3d g(0, -9.81, 0);

#endif // STALE_WEKTORY_H