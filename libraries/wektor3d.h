#ifndef WEKTOR3D_H
#define WEKTOR3D_H
#include <math.h>

#include <iostream>

using namespace std;

// Biblioteka z bazowymi operacjami na wektorze trójwymiarowym.
// Oczywiście operacje te powinny działać dla wektora dwuwymiarowego.

class wektor3d {
 private:
  double x, y, z;

 public:
  wektor3d(double _x = 0.0, double _y = 0.0, double _z = 0.0)
      : x(_x), y(_y), z(_z) {}

  double getX() const { return x; }
  double getY() const { return y; }
  double getZ() const { return z; }
  wektor3d operator+(wektor3d drugi) const {
    return wektor3d(x + drugi.getX(), y + drugi.getY(), z + drugi.getZ());
  }
  wektor3d operator-(wektor3d drugi) const {
    return wektor3d(x - drugi.getX(), y - drugi.getY(), z - drugi.getZ());
  }
  wektor3d operator*(double a) const { return wektor3d(a * x, a * y, a * z); }
  wektor3d operator/(double a) const { return (*this) * (1 / a); }
  double operator*(wektor3d a) const {
    return double(x * a.getX() + y * a.getY() + z * a.getZ());
  }
  double dlugosc() const { return sqrt((*this) * (*this)); }
  wektor3d normuj() const {
    if (dlugosc()) return (*this) / dlugosc();
  }
  double odleglosc(wektor3d drugi) const { return ((*this) - drugi).dlugosc(); }
  wektor3d iloczynWektorowy(wektor3d drugi) const {
    return wektor3d(y * drugi.getZ() - z * drugi.getY(), z * drugi.getX() - x * drugi.getZ(),
                    x * drugi.getY() - y * drugi.getX());
  }
  wektor3d operator%(wektor3d drugi) const { return iloczynWektorowy(drugi); }
  void wydrukuj(string name = "") const {
    if (!name.empty()) cout << "Wektor \"" << name << "\":";
    cout << "   ( " << x << ", " << y << ", " << z << " )" << endl;
  }
};

extern const wektor3d zeroWektor(0, 0, 0);

wektor3d operator*(double a, wektor3d &wektor) { return wektor * a; }

#endif  // WEKTOR3D_H