#ifndef PRZESZKODY_H
#define PRZESZKODY_H

#include "punkt.h"
#include "kulki.h"
#include "wektor3d.h"

class przeszkoda {
 public:
  virtual bool zderzenie(const punkt) const {};
  virtual void zderzonyTik(punkt &) const {};
  virtual bool zderzenie(const kulka) const {};
  virtual void zderzonyTik(kulka &) const {};
};

class klatka2d : public przeszkoda {
 protected:
  double X_lewy, X_prawy, Y_gora, Y_dol;

 public:
  klatka2d(double _x_l, double _x_p, double _y_g, double _y_d)
      : X_lewy(_x_l), X_prawy(_x_p), Y_gora(_y_g), Y_dol(_y_d) {}
  virtual bool zderzenie(const punkt Punkt) const override {
    return Punkt.getPolozenie().getX() < X_lewy ||
           Punkt.getPolozenie().getX() > X_prawy ||
           Punkt.getPolozenie().getY() > Y_gora ||
           Punkt.getPolozenie().getY() < Y_dol;
  }
  virtual void zderzonyTik(punkt &Punkt) const override {
    if (Punkt.getPolozenie().getX() < X_lewy) {
      Punkt.setPolozenie(
          wektor3d(Punkt.getPolozenie().getX() -
                       2 * (Punkt.getPolozenie().getX() - X_lewy),
                   Punkt.getPolozenie().getY(), Punkt.getPolozenie().getZ()));
      Punkt.setV(wektor3d(-Punkt.getV().getX(), Punkt.getV().getY(),
                          Punkt.getV().getZ()));
    }
    if (Punkt.getPolozenie().getX() > X_prawy) {
      Punkt.setPolozenie(
          wektor3d(Punkt.getPolozenie().getX() -
                       2 * (Punkt.getPolozenie().getX() - X_prawy),
                   Punkt.getPolozenie().getY(), Punkt.getPolozenie().getZ()));
      Punkt.setV(wektor3d(-Punkt.getV().getX(), Punkt.getV().getY(),
                          Punkt.getV().getZ()));
    }
    if (Punkt.getPolozenie().getY() > Y_gora) {
      Punkt.setPolozenie(
          wektor3d(Punkt.getPolozenie().getX(),
                   Punkt.getPolozenie().getY() -
                       2 * (Punkt.getPolozenie().getY() - Y_gora),
                   Punkt.getPolozenie().getZ()));
      Punkt.setV(wektor3d(Punkt.getV().getX(), -Punkt.getV().getY(),
                          Punkt.getV().getZ()));
    }
    if (Punkt.getPolozenie().getY() < Y_dol) {
      Punkt.setV(zeroWektor);
      Punkt.setPolozenie(wektor3d(Punkt.getPolozenie().getX(), Y_dol,
                                  Punkt.getPolozenie().getZ()));
    }
  }
  virtual bool zderzenie(const kulka Kulka) const override {
    return Kulka.getPolozenie().getX()-Kulka.getR() < X_lewy ||
           Kulka.getPolozenie().getX()+Kulka.getR() > X_prawy ||
           Kulka.getPolozenie().getY()+Kulka.getR() > Y_gora ||
           Kulka.getPolozenie().getY()-Kulka.getR() < Y_dol;
  }
  virtual void zderzonyTik(kulka &Kulka) const override {
    if (Kulka.getPolozenie().getX()-Kulka.getR() < X_lewy) {
      Kulka.setPolozenie(
          wektor3d(Kulka.getPolozenie().getX() -
                       2 * (Kulka.getPolozenie().getX() - X_lewy),
                   Kulka.getPolozenie().getY(), Kulka.getPolozenie().getZ()));
      Kulka.setV(wektor3d(-Kulka.getV().getX(), Kulka.getV().getY(),
                          Kulka.getV().getZ()));
    }
    if (Kulka.getPolozenie().getX()+Kulka.getR() > X_prawy) {
      Kulka.setPolozenie(
          wektor3d(Kulka.getPolozenie().getX() -
                       2 * (Kulka.getPolozenie().getX() - X_prawy),
                   Kulka.getPolozenie().getY(), Kulka.getPolozenie().getZ()));
      Kulka.setV(wektor3d(-Kulka.getV().getX(), Kulka.getV().getY(),
                          Kulka.getV().getZ()));
    }
    if (Kulka.getPolozenie().getY()+Kulka.getR() > Y_gora) {
      Kulka.setPolozenie(
          wektor3d(Kulka.getPolozenie().getX(),
                   Kulka.getPolozenie().getY() -
                       2 * (Kulka.getPolozenie().getY() - Y_gora),
                   Kulka.getPolozenie().getZ()));
      Kulka.setV(wektor3d(Kulka.getV().getX(), -Kulka.getV().getY(),
                          Kulka.getV().getZ()));
    }
    if (Kulka.getPolozenie().getY()-Kulka.getR() < Y_dol) {
      Kulka.setV(zeroWektor);
      Kulka.setPolozenie(wektor3d(Kulka.getPolozenie().getX(), Y_dol,
                                  Kulka.getPolozenie().getZ()));
    }
  }
};

class przeszkodaSfera : public przeszkoda {
 protected:
  double r;
  wektor3d pozycja;
  double tlumienie;

 public:
  przeszkodaSfera(double _r, wektor3d _pozycja, double _tlumienie = 1.0)
      : r(_r), pozycja(_pozycja), tlumienie(_tlumienie) {}
  virtual bool zderzenie(const punkt Punkt) const override {
    return pozycja.odleglosc(Punkt.getPolozenie()) < r;
  }
  virtual void zderzonyTik(punkt &Punkt) const override {
    // licz punkt
    wektor3d przesuniecie = Punkt.getPolozenie() - pozycja;
    wektor3d n = przesuniecie.normuj();
    Punkt.setPolozenie(Punkt.getPolozenie() +
                       2 * n * (r - przesuniecie.dlugosc()));
    wektor3d Vn = n * (n * Punkt.getV());
    Punkt.setV(Punkt.getV() - Vn - Vn * tlumienie);
  }
  virtual bool zderzenie(const kulka Kulka) const override {
    return pozycja.odleglosc(Kulka.getPolozenie()) < r+Kulka.getR();
  }
  virtual void zderzonyTik(kulka &Kulka) const override {
    // licz punkt
    wektor3d przesuniecie = Kulka.getPolozenie() - pozycja;
    wektor3d n = przesuniecie.normuj();
    Kulka.setPolozenie(Kulka.getPolozenie() +
                       2 * n * (r - przesuniecie.dlugosc()));
    wektor3d Vn = n * (n * Kulka.getV());
    Kulka.setV(Kulka.getV() - Vn - Vn * tlumienie);
  }
};

#endif  // PRZESZKODY_H