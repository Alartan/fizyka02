#ifndef GLPRZESZKODY_H
#define GLPRZESZKODY_H

#include <GL/freeglut.h>
#include <math.h>

#include "GLobjects.h"
#include "przeszkody.h"

class GLklatka2d : public GLobject, public klatka2d {
 public:
  GLklatka2d(double _x_l, double _x_p, double _y_g, double _y_d)
      : klatka2d(_x_l, _x_p, _y_g, _y_d) {}
  void paintObject() const override {
    glLineWidth(10.0f);
    glBegin(GL_LINE_LOOP);
    glColor3d(0.9059, 0.07421875, 0.46484375);
    glVertex3d(X_lewy, Y_gora, 0);
    glVertex3d(X_lewy, Y_dol, 0);
    glVertex3d(X_prawy, Y_dol, 0);
    glVertex3d(X_prawy, Y_gora, 0);
    glEnd();
  }
};

class GLsfera : public GLobject, public przeszkodaSfera {
 public:
  GLsfera(double _r, wektor3d _pozycja, double _tlumienie = 1.0)
      : przeszkodaSfera(_r, _pozycja, _tlumienie) {}

  void paintObject() const override {
    glLineWidth(1.0f);
    glBegin(GL_POLYGON);
    glColor3d(0.5, 0.1, 0.1);
    for (double i = 0; i<=2*M_PI; i=i+2*M_PI/360/r) {
      glVertex3d(r*cos(i)+pozycja.getX(), r*sin(i)+pozycja.getY(), 0);
    }
    glEnd();}
};

#endif /* GLPRZESZKODY_H */
