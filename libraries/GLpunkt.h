#ifndef GLPUNKT_H
#define GLPUNKT_H
#include <GL/freeglut.h>

#include "GLobjects.h"
#include "punkt.h"
#include "kulki.h"

class GLkulka : public GLobject, public kulka {
  double r, g, b;

 public:
  GLkulka(wektor3d _polozenie, wektor3d _v = zeroWektor,
          wektor3d _a = zeroWektor, double _m = 0, double _R=0, double _r = 0, double _g = 0,
          double _b = 0)
      : kulka(_polozenie, _v, _a, _m, _R), r(_r), g(_g), b(_b) {}

  void paintObject() const override {
    glBegin(GL_POLYGON);
    glColor3d(r, g, b);
    for (double i = 0; i<=2*M_PI; i=i+2*M_PI/360/R) {
      glVertex3d(R*cos(i)+getPolozenie().getX(), R*sin(i)+getPolozenie().getY(), 0);
    }
    glEnd();
  }
};

class GLpunkt : public GLobject, public punkt {
  double r, g, b;

 public:
  GLpunkt(wektor3d _polozenie, wektor3d _v = zeroWektor,
          wektor3d _a = zeroWektor, double _m = 0, double _r = 0, double _g = 0,
          double _b = 0)
      : punkt(_polozenie, _v, _a, _m), r(_r), g(_g), b(_b) {}

  void setKolor(double _r, double _g, double _b) {
    r = _r;
    g = _g;
    b = _b;
  }
  void paintObject() const override {
    glPointSize(10.0f);
    glBegin(GL_POINTS);
    glColor3d(r, g, b);
    glVertex3d(getPolozenie().getX(), getPolozenie().getY(),
               getPolozenie().getZ());
    glEnd();
  }
};

#endif /* GLPUNKT_H */
