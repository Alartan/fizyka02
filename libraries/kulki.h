#ifndef KULKI_H
#define KULKI_H

#include "punkt.h"

class kulka : public punkt {
  protected:
    double R;
  public:
    kulka(wektor3d _polozenie, wektor3d _v = zeroWektor, wektor3d _a = zeroWektor,
        double _m = 0, double _r = 0) : punkt(_polozenie, _v, _a, _m), R(_r) {}
    double getR() const {return R;}
};

#endif /* KULKI_H */
