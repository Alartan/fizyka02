#ifndef PUNKT_H
#define PUNKT_H

#include "wektor3d.h"

class punkt {
 protected:
  wektor3d polozenie, v, a;
  double m;

 public:
  punkt(wektor3d _polozenie, wektor3d _v = zeroWektor, wektor3d _a = zeroWektor,
        double _m = 0)
      : polozenie(_polozenie), v(_v), a(_a), m(_m) {}
  virtual void wydrukuj() const {
    getPolozenie().wydrukuj("Położenie");
    getV().wydrukuj("Prędkość");
    getA().wydrukuj("Przyspieszenie");
    cout << "Masa: " << m << endl;
  }
  virtual wektor3d getPolozenie() const { return polozenie; }
  virtual void setPolozenie(const wektor3d _polozenie) { polozenie = _polozenie; }
  virtual wektor3d getV() const { return v; }
  virtual void setV(const wektor3d _v) { v = _v; }
  virtual wektor3d getP() const { return getV() * m; }
  void setP(const wektor3d _p) { v = _p / m; }
  virtual wektor3d getA() const { return a; }
  virtual void setA(const wektor3d _a) { a = _a; }
  virtual wektor3d getF() const { return getA() * m; }
  virtual void setF(const wektor3d _F) { a = _F / m; }
  virtual void tikEuler(double t) {
    v = v + a * t;
    polozenie = polozenie + v * t;
  }
  virtual void tikMidPoint(double t) {
    tikEuler(t / 2);
    tikEuler(t / 2);
  }
};

#endif  // PUNKT_H